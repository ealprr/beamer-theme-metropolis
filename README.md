Beamer metropolis theme
=======================

**Metropolis** is a LaTeX Beamer theme with minimal visual based on original [mtheme](https://github.com/matze/mtheme) from Matthias Vogelgesang.

The core design principles of the theme were described in a [blog post](http://bloerg.net/2014/09/20/a-modern-beamer-theme.html).

A preview of the theme is available [here](https://goo.gl/5LL0BM).


## Installation

To install the theme, copy the style files ending with `.sty` to the source files of your presentation.


## Customization

### Package options

The `usetitleprogressbar` option adds a thin progress bar similar to the section progress bar underneath *each* frame title

In order to use `\cite`, `\ref` and similar commands in a frame title you have
to protect the title. This can be done automatically with the
`protectframetitle` option.

The `blockbg` option defines extra colors used in defining the blocks.
The blocks then have a gray background similar to other beamer themes.


### License

The theme itself is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). This
means that if you change the theme and re-distribute it, you *must* retain the
copyright notice header and license it under the same CC-BY-SA license. This
does not affect the presentation that you create with the theme.
